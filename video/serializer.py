from rest_framework.serializers import ModelSerializer
from .models import Video
from django.contrib.auth.models import User
class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')
class VideoSerializer(ModelSerializer):
    user = UserSerializer(read_only=True)
    class Meta:
        model = Video
        fields = ('id','title', 'description', 'user')

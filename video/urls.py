from django.urls import path
from . import views


urlpatterns = [
    path('videos/', views.ListVideo.as_view(),name="videos-lista"),
    path('videos/<int:pk>', views.DetailVideo.as_view(),name="detail-video"),
]